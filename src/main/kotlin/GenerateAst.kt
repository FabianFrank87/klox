import java.io.PrintWriter
import kotlin.system.exitProcess

/**
 * Unused and untested, but created while following along chapter 5.3.2. Instead of code-generating the Visitor pattern,
 * we use data classes in a sealed class hierarchy. See [de.pagefault.lox.Stmt] and [de.pagefault.lox.Expr].
 */

fun main(args: Array<String>) {

    if (args.isEmpty()) {
        System.err.println("Usage: generate_ast <output directory>")
        exitProcess(64)
    }

    val outputDir = args[0]

    defineAst(
        outputDir, "Expr", listOf(
            "Binary   : Expr left, Token operator, Expr right",
            "Grouping : Expr expression",
            "Literal  : Object value",
            "Unary    : Token operator, Expr right"
        )
    )
}

private fun defineAst(outputDir: String, baseName: String, types: List<String>) {
    val path = "$outputDir/$baseName.java"
    val writer = PrintWriter(path, "UTF-8")
    writer.println("package com.craftinginterpreters.lox;")
    writer.println()
    writer.println("import java.util.List;")
    writer.println()
    writer.println("abstract class $baseName {")

    defineVisitor(writer, baseName, types)

    // The AST classes.
    for (type in types) {
        val className = type.split(":")[0].trim { it <= ' ' }
        val fields = type.split(":")[1].trim { it <= ' ' }
        defineType(writer, baseName, className, fields)
    }

    // The base accept() method.
    writer.println()
    writer.println("  abstract <R> R accept(Visitor<R> visitor);")

    writer.println("}")
    writer.close()
}

private fun defineVisitor(writer: PrintWriter, baseName: String, types: List<String>) {
    writer.println("  interface Visitor<R> {")
    for (type: String in types) {
        val typeName = type.split(":")[0].trim { it <= ' ' }
        writer.println("    R visit$typeName$baseName($typeName ${baseName.toLowerCase()});")
    }
    writer.println("  }")
}

private fun defineType(
    writer: PrintWriter, baseName: String,
    className: String, fieldList: String
) {
    writer.println("  static class $className extends $baseName {")

    // Constructor.
    writer.println("    $className($fieldList) {")

    // Store parameters in fields.
    val fields = fieldList.split(", ")
    for (field: String in fields) {
        val name = field.split(" ")[1]
        writer.println("      this.$name = $name;")
    }
    writer.println("    }")

    // Visitor pattern.
    writer.println()
    writer.println("    @Override")
    writer.println("    <R> R accept(Visitor<R> visitor) {")
    writer.println("      return visitor.visit$className$baseName(this);")
    writer.println("    }")

    // Fields.
    writer.println()
    for (field: String in fields) {
        writer.println("    final $field;")
    }
    writer.println("  }")
}