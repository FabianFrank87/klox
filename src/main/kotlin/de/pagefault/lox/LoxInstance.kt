package de.pagefault.lox

class LoxInstance(private val klass: LoxClass) {
    private val fields: MutableMap<String, Any?> = mutableMapOf()

    operator fun get(name: Token): Any? =
        if (fields.containsKey(name.lexeme)) {
            fields[name.lexeme]
        } else {
            klass.findMethod(name.lexeme)?.bind(this) ?: throw RuntimeError(
                name,
                "Undefined property '" + name.lexeme + "'."
            )
        }

    operator fun set(name: Token, value: Any?) {
        fields[name.lexeme] = value
    }

    override fun toString(): String {
        return "<obj ${klass.name}>"
    }
}
