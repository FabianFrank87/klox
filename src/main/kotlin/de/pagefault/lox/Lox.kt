package de.pagefault.lox

import java.io.BufferedReader
import java.io.InputStreamReader
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.system.exitProcess

object Lox {
    private val interpreter = Interpreter()
    private var hadError = false
    private var hadRuntimeError = false

    fun main(args: Array<String>) {
        when {
            args.size > 1 -> {
                println("Usage: jlox [script]")
                exitProcess(64)
            }
            args.size == 1 -> {
                runFile(args[0])
            }
            else -> {
                runPrompt()
            }
        }
    }

    private fun runFile(path: String) {
        run(Files.readString(Paths.get(path)), false)

        // Indicate an error in the exit code.
        if (hadError) exitProcess(65)
        if (hadRuntimeError) exitProcess(70)
    }

    private fun runPrompt() {
        val input = InputStreamReader(System.`in`)
        val reader = BufferedReader(input)
        while (true) {
            print("> ")
            val line = reader.readLine() ?: break
            run(line, true)
            hadError = false
        }
    }

    private fun run(source: String, printExpression: Boolean) {
        val scanner = Scanner(source)
        val tokens: List<Token> = scanner.scanTokens()

        val parser = Parser(tokens)
        var expression = parser.parse()

        // Stop if there was a syntax error.
        if (hadError) return

        if (interpreter.debug) {
            println(AstPrinter().print(expression))
        }

        // Wrap trailing expressions in a print statement, for REPL convenience
        if (printExpression) {
            expression = expression.mapIndexed { i, e ->
                if (i == expression.size - 1 && e is Expression) {
                    Print(e.expression)
                } else {
                    e
                }
            }
        }

        Resolver(interpreter).resolve(expression)

        // Stop if there was a resolution error.
        if (hadError) return

        interpreter.interpret(expression)
    }

    fun error(line: Int, message: String) {
        report(line, "", message)
    }

    private fun report(line: Int, where: String, message: String) {
        System.err.println("[line $line] Error$where: $message")
        hadError = true
    }

    fun error(token: Token, message: String?) {
        if (token.type === TokenType.EOF) {
            report(token.line, " at end", message!!)
        } else {
            report(token.line, " at '" + token.lexeme + "'", message!!)
        }
    }

    fun runtimeError(error: RuntimeError) {
        System.err.println("${error.message}\n[line ${error.token.line}]")
        hadRuntimeError = true
    }
}
