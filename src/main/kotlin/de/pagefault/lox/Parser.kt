package de.pagefault.lox

import de.pagefault.lox.TokenType.AND
import de.pagefault.lox.TokenType.BANG
import de.pagefault.lox.TokenType.BANG_EQUAL
import de.pagefault.lox.TokenType.CLASS
import de.pagefault.lox.TokenType.COMMA
import de.pagefault.lox.TokenType.DOT
import de.pagefault.lox.TokenType.ELSE
import de.pagefault.lox.TokenType.EOF
import de.pagefault.lox.TokenType.EQUAL
import de.pagefault.lox.TokenType.EQUAL_EQUAL
import de.pagefault.lox.TokenType.FALSE
import de.pagefault.lox.TokenType.FOR
import de.pagefault.lox.TokenType.FUN
import de.pagefault.lox.TokenType.GREATER
import de.pagefault.lox.TokenType.GREATER_EQUAL
import de.pagefault.lox.TokenType.IDENTIFIER
import de.pagefault.lox.TokenType.IF
import de.pagefault.lox.TokenType.LEFT_BRACE
import de.pagefault.lox.TokenType.LEFT_PAREN
import de.pagefault.lox.TokenType.LESS
import de.pagefault.lox.TokenType.LESS_EQUAL
import de.pagefault.lox.TokenType.MINUS
import de.pagefault.lox.TokenType.NIL
import de.pagefault.lox.TokenType.NUMBER
import de.pagefault.lox.TokenType.OR
import de.pagefault.lox.TokenType.PLUS
import de.pagefault.lox.TokenType.PRINT
import de.pagefault.lox.TokenType.RETURN
import de.pagefault.lox.TokenType.RIGHT_BRACE
import de.pagefault.lox.TokenType.RIGHT_PAREN
import de.pagefault.lox.TokenType.SEMICOLON
import de.pagefault.lox.TokenType.SLASH
import de.pagefault.lox.TokenType.STAR
import de.pagefault.lox.TokenType.STRING
import de.pagefault.lox.TokenType.SUPER
import de.pagefault.lox.TokenType.THIS
import de.pagefault.lox.TokenType.TRUE
import de.pagefault.lox.TokenType.VAR
import de.pagefault.lox.TokenType.WHILE

/**
 * expression     → equality ;
 * equality       → comparison ( ( "!=" | "==" ) comparison )* ;
 * comparison     → addition ( ( ">" | ">=" | "<" | "<=" ) addition )* ;
 * addition       → multiplication ( ( "-" | "+" ) multiplication )* ;
 * multiplication → unary ( ( "/" | "*" ) unary )* ;
 * unary          → ( "!" | "-" ) unary
 *                | primary ;
 * primary        → NUMBER | STRING | "false" | "true" | "nil"
 *                | "(" expression ")" ;
 */
class Parser(private val tokens: List<Token>) {
    private class ParseError : RuntimeException()

    private var current = 0

    fun parse(): List<Stmt> {
        val statements: MutableList<Stmt> = mutableListOf()
        while (!isAtEnd()) {
            declaration()?.let { statements.add(it) }
        }

        return statements
    }

    private fun expression(): Expr {
        return assignment()
    }

    private fun declaration(): Stmt? =
        try {
            when {
                match(CLASS) -> classDeclaration()
                match(FUN) -> function("function")
                match(VAR) -> varDeclaration()
                else -> statement()
            }
        } catch (error: ParseError) {
            synchronize()
            null
        }

    private fun classDeclaration(): Stmt {
        val name = consume(IDENTIFIER, "Expect class name.")

        val superclass = if (match(LESS)) {
            consume(IDENTIFIER, "Expect superclass name.")
            Variable(previous())
        } else {
            null
        }

        consume(LEFT_BRACE, "Expect '{' before class body.")

        val methods: MutableList<Function> = mutableListOf()
        while (!check(RIGHT_BRACE) && !isAtEnd()) {
            methods.add(function("method"))
        }

        consume(RIGHT_BRACE, "Expect '}' after class body.")
        return Class(name, superclass, methods)
    }

    private fun function(kind: String): Function {
        val name = consume(IDENTIFIER, "Expect $kind name.")

        consume(LEFT_PAREN, "Expect '(' after $kind name.")
        val parameters: MutableList<Token> = mutableListOf()
        if (!check(RIGHT_PAREN)) {
            do {
                if (parameters.size >= 255) {
                    error(peek(), "Cannot have more than 255 parameters.")
                }
                parameters.add(consume(IDENTIFIER, "Expect parameter name."))
            } while (match(COMMA))
        }
        consume(RIGHT_PAREN, "Expect ')' after parameters.")

        consume(LEFT_BRACE, "Expect '{' before $kind body.")
        val body = block()
        return Function(name, parameters, body)
    }

    private fun statement(): Stmt =
        when {
            match(FOR) -> forStatement()
            match(IF) -> ifStatement()
            match(PRINT) -> printStatement()
            match(RETURN) -> returnStatement()
            match(WHILE) -> whileStatement()
            match(LEFT_BRACE) -> Block(block())
            else -> expressionStatement()
        }

    private fun forStatement(): Stmt {
        consume(LEFT_PAREN, "Expect '(' after 'for'.")

        val initializer: Stmt? = when {
            match(SEMICOLON) -> null
            match(VAR) -> varDeclaration()
            else -> expressionStatement()
        }

        val condition: Expr? =
            if (check(SEMICOLON)) {
                null
            } else {
                expression()
            }
        consume(SEMICOLON, "Expect ';' after loop condition.")

        val increment: Expr? =
            if (check(RIGHT_PAREN)) {
                null
            } else {
                expression()
            }
        consume(RIGHT_PAREN, "Expect ')' after for clauses.")

        var body = statement()

        /*
         * desugaring to:
         *
         * initializer;
         * while (condition) {
         *   body
         *   increment
         * }
         */
        if (increment != null) {
            body = Block(listOf(body, Expression(increment)))
        }

        body = While(condition ?: Literal(true), body)

        if (initializer != null) {
            body = Block(listOf(initializer, body))
        }

        return body
    }

    private fun whileStatement(): Stmt {
        consume(LEFT_PAREN, "Expect '(' after 'while'.")
        val condition = expression()
        consume(RIGHT_PAREN, "Expect ')' after condition.")
        val body = statement()
        return While(condition, body)
    }

    private fun ifStatement(): Stmt {
        consume(LEFT_PAREN, "Expect '(' after 'if'.")
        val condition = expression()
        consume(RIGHT_PAREN, "Expect ')' after if condition.")
        val thenBranch = statement()
        val elseBranch: Stmt? = if (match(ELSE)) {
            statement()
        } else {
            null
        }
        return If(condition, thenBranch, elseBranch)
    }

    private fun printStatement(): Stmt {
        val value = expression()
        consume(SEMICOLON, "Expect ';' after value.")
        return Print(value)
    }

    private fun returnStatement(): Stmt {
        val keyword = previous()
        val value: Expr? = if (!check(SEMICOLON)) expression() else null

        consume(SEMICOLON, "Expect ';' after return value.")
        return Return(keyword, value)
    }

    private fun expressionStatement(): Stmt {
        val expr = expression()
        consume(SEMICOLON, "Expect ';' after expression.")
        return Expression(expr)
    }

    private fun block(): List<Stmt> {
        val statements: MutableList<Stmt> = mutableListOf()
        while (!check(RIGHT_BRACE) && !isAtEnd()) {
            declaration()?.let { statements.add(it) }
        }
        consume(RIGHT_BRACE, "Expect '}' after block.")
        return statements
    }

    private fun varDeclaration(): Stmt? {
        val name = consume(IDENTIFIER, "Expect variable name.")
        val initializer: Expr? = if (match(EQUAL)) {
            expression()
        } else {
            null
        }
        consume(SEMICOLON, "Expect ';' after variable declaration.")
        return Var(name, initializer)
    }

    private fun assignment(): Expr {
        val expr = or()

        return if (match(EQUAL)) {
            val equals = previous()
            val value = assignment()
            when (expr) {
                is Variable -> Assign(expr.name, value)
                is Get -> Set(expr.`object`, expr.name, value)
                else -> {
                    error(equals, "Invalid assignment target.")
                    expr
                }
            }
        } else {
            expr
        }
    }

    private fun or(): Expr {
        var expr: Expr = and()
        while (match(OR)) {
            val operator = previous()
            val right: Expr = and()
            expr = Logical(expr, operator, right)
        }
        return expr
    }

    private fun and(): Expr {
        var expr = equality()
        while (match(AND)) {
            val operator = previous()
            val right = equality()
            expr = Logical(expr, operator, right)
        }
        return expr
    }

    private fun equality(): Expr {
        var expr: Expr = comparison()
        while (match(BANG_EQUAL, EQUAL_EQUAL)) {
            val operator: Token = previous()
            val right: Expr = comparison()
            expr = Binary(expr, operator, right)
        }
        return expr
    }

    private fun comparison(): Expr {
        var expr: Expr = addition()
        while (match(GREATER, GREATER_EQUAL, LESS, LESS_EQUAL)) {
            val operator = previous()
            val right: Expr = addition()
            expr = Binary(expr, operator, right)
        }
        return expr
    }

    private fun addition(): Expr {
        var expr = multiplication()
        while (match(MINUS, PLUS)) {
            val operator = previous()
            val right = multiplication()
            expr = Binary(expr, operator, right)
        }
        return expr
    }

    private fun multiplication(): Expr {
        var expr: Expr = unary()
        while (match(SLASH, STAR)) {
            val operator = previous()
            val right: Expr = unary()
            expr = Binary(expr, operator, right)
        }
        return expr
    }

    private fun unary(): Expr {
        if (match(BANG, MINUS)) {
            val operator = previous()
            val right = unary()
            return Unary(operator, right)
        }
        return call()
    }

    private fun call(): Expr {
        var expr: Expr = primary()
        while (true) {
            expr = if (match(LEFT_PAREN)) {
                finishCall(expr)
            } else if (match(DOT)) {
                val name = consume(IDENTIFIER, "Expect property name after '.'.")
                Get(expr, name)
            } else {
                break
            }
        }
        return expr
    }

    private fun finishCall(callee: Expr): Expr {
        val arguments: MutableList<Expr> = mutableListOf()
        if (!check(RIGHT_PAREN)) {
            do {
                if (arguments.size >= 255) {
                    error(peek(), "Cannot have more than 255 arguments.")
                }
                arguments.add(expression())
            } while (match(COMMA))
        }
        val paren = consume(RIGHT_PAREN, "Expect ')' after arguments.")
        return Call(callee, paren, arguments)
    }

    private fun primary() = when {
        match(FALSE) -> Literal(false)
        match(TRUE) -> Literal(true)
        match(NIL) -> Literal(null)
        match(NUMBER, STRING) -> Literal(previous().literal)
        match(SUPER) -> {
            val keyword = previous()
            consume(DOT, "Expect '.' after 'super'.")
            val method = consume(
                IDENTIFIER,
                "Expect superclass method name."
            )
            Super(keyword, method)
        }
        match(THIS) -> This(previous())
        match(IDENTIFIER) -> Variable(previous())
        match(LEFT_PAREN) -> {
            val expr = expression()
            consume(RIGHT_PAREN, "Expect ')' after expression.")
            Grouping(expr)
        }
        else -> throw error(peek(), "Expect expression.")
    }

    private fun match(vararg types: TokenType): Boolean {
        for (type in types) {
            if (check(type)) {
                advance()
                return true
            }
        }
        return false
    }

    private fun consume(type: TokenType, message: String): Token {
        if (check(type)) return advance()
        throw error(peek(), message)
    }

    private fun check(type: TokenType) =
        if (isAtEnd()) false else peek().type === type

    private fun advance(): Token {
        if (!isAtEnd()) current++
        return previous()
    }

    private fun isAtEnd() = peek().type === EOF

    private fun peek() = tokens[current]

    private fun previous() = tokens[current - 1]

    private fun error(token: Token, message: String): ParseError {
        Lox.error(token, message)
        return ParseError()
    }

    private fun synchronize() {
        advance()
        while (!isAtEnd()) {
            if (previous().type === SEMICOLON) return
            @Suppress("NON_EXHAUSTIVE_WHEN")
            when (peek().type) {
                CLASS, FUN, VAR, FOR, IF, WHILE, PRINT, RETURN -> return
            }
            advance()
        }
    }
}