package de.pagefault.lox

import de.pagefault.lox.Lox.runtimeError
import de.pagefault.lox.TokenType.BANG
import de.pagefault.lox.TokenType.BANG_EQUAL
import de.pagefault.lox.TokenType.EQUAL_EQUAL
import de.pagefault.lox.TokenType.GREATER
import de.pagefault.lox.TokenType.GREATER_EQUAL
import de.pagefault.lox.TokenType.LESS
import de.pagefault.lox.TokenType.LESS_EQUAL
import de.pagefault.lox.TokenType.MINUS
import de.pagefault.lox.TokenType.PLUS
import de.pagefault.lox.TokenType.SLASH
import de.pagefault.lox.TokenType.STAR
import java.math.BigDecimal
import java.time.LocalDateTime
import java.time.ZoneOffset

class Interpreter {
    private val globals = Environment()
    private var environment = globals
    private val locals: MutableMap<Expr, Int> = mutableMapOf()
    internal var debug: Boolean = false

    init {
        globals.define("clock", object : LoxCallable {
            override fun arity(): Int = 0

            override fun call(interpreter: Interpreter, arguments: List<Any?>): Double =
                LocalDateTime.now().toEpochSecond(ZoneOffset.UTC).toDouble()
        })

        globals.define("debug", object : LoxCallable {
            override fun arity(): Int = 1

            override fun call(interpreter: Interpreter, arguments: List<Any?>): Boolean {
                debug = isTruthy(arguments[0])
                return debug
            }
        })
    }

    fun interpret(statements: List<Stmt>) {
        try {
            for (statement in statements) {
                execute(statement)
            }
        } catch (error: RuntimeError) {
            runtimeError(error)
        }
    }

    private fun execute(stmt: Stmt): Unit =
        when (stmt) {
            is Expression -> {
                evaluate(stmt.expression)
                Unit
            }
            is Print -> {
                val value = evaluate(stmt.expression)
                println(stringify(value))
            }
            is Var -> environment.define(stmt.name.lexeme, stmt.initializer?.let { evaluate(it) })
            is Function -> environment.define(stmt.name.lexeme, LoxFunction(stmt, environment, false))
            is Block -> executeBlock(stmt.statements, Environment(environment))
            is If -> {
                if (isTruthy(evaluate(stmt.condition))) {
                    execute(stmt.thenBranch)
                } else if (stmt.elseBranch != null) {
                    execute(stmt.elseBranch)
                }
                @Suppress("RedundantUnitExpression")
                Unit
            }
            is While -> while (isTruthy(evaluate(stmt.condition))) execute(stmt.body)
            is Return -> throw ReturnException(stmt.value?.let { evaluate(it) })
            is Class -> `class`(stmt)
        }

    private fun `class`(stmt: Class) {
        val superclass: LoxClass? = stmt.superclass?.let {
            val maybeSuperclass = evaluate(it)
            if (maybeSuperclass is LoxClass) {
                maybeSuperclass
            } else {
                throw RuntimeError(
                    it.name, "Superclass must be a class."
                )
            }
        }

        environment.define(stmt.name.lexeme, null)

        stmt.superclass?.let {
            environment = Environment(environment)
            environment.define("super", superclass)
        }

        val methods = stmt.methods.map {
            it.name.lexeme to LoxFunction(it, environment, it.name.lexeme == "init")
        }.toMap()

        val klass = LoxClass(stmt.name.lexeme, superclass, methods)

        stmt.superclass?.let { environment = environment.enclosing!! }

        environment.assign(stmt.name, klass)
    }

    fun resolve(expr: Expr, depth: Int) {
        locals[expr] = depth
    }

    @Suppress("RedundantUnitReturnType")
    fun executeBlock(statements: List<Stmt>, environment: Environment): Unit {
        val previous = this.environment
        try {
            this.environment = environment
            for (statement in statements) {
                execute(statement)
            }
        } finally {
            this.environment = previous
        }
    }

    private fun evaluate(expr: Expr): Any? =
        when (expr) {
            is Binary -> binary(expr)
            is Grouping -> evaluate(expr.expression)
            is Literal -> expr.value
            is Unary -> unary(expr)
            is Variable -> lookUpVariable(expr.name, expr)
            is Assign -> assign(expr)
            is Logical -> logical(expr)
            is Call -> call(expr)
            is Get -> get(expr)
            is Set -> set(expr)
            is This -> lookUpVariable(expr.keyword, expr)
            is Super -> `super`(expr)
        }

    private fun `super`(expr: Super): Any? {
        val distance = locals[expr] ?: throw RuntimeError(expr.keyword, "Failed to find super in Environment.")
        val superclass = environment.getAt(
            distance, "super"
        ) as LoxClass

        // "this" is always one level nearer than "super"'s environment.
        val `object` = environment.getAt(distance - 1, "this") as LoxInstance

        val method = superclass.findMethod(expr.method.lexeme) ?: throw RuntimeError(
            expr.method,
            "Undefined property '" + expr.method.lexeme + "'."
        )

        return method.bind(`object`)
    }

    private fun set(expr: Set): Any? {
        val `object` = evaluate(expr.`object`)
        if (`object` == null || `object` !is LoxInstance) {
            throw RuntimeError(expr.name, "Only instances have fields.")
        }

        val value = evaluate(expr.value)
        `object`[expr.name] = value
        return value
    }

    private fun get(expr: Get): Any? {
        val `object` = evaluate(expr.`object`)
        if (`object` is LoxInstance) {
            return `object`[expr.name]
        }

        throw RuntimeError(expr.name, "Only instances have properties.")
    }

    private fun assign(expr: Assign): Any? {
        val value = evaluate(expr.value)

        val distance = locals[expr]
        if (distance != null) {
            environment.assignAt(distance, expr.name, value)
        } else {
            environment.assign(expr.name, value)
        }

        return value
    }

    private fun lookUpVariable(name: Token, expr: Expr): Any? {
        val distance = locals[expr]
        return if (distance != null) {
            environment.getAt(distance, name.lexeme)
        } else {
            globals[name]
        }
    }

    private fun call(expr: Call): Any? {
        val callee = evaluate(expr.callee)
        val arguments = expr.arguments.map { evaluate(it) }

        if (callee == null || callee !is LoxCallable) {
            throw RuntimeError(
                expr.paren,
                "Can only call functions and classes."
            )
        }

        if (arguments.size != callee.arity()) {
            throw RuntimeError(
                expr.paren,
                "Expected ${callee.arity()} arguments but got ${arguments.size}."
            )
        }


        return callee.call(this, arguments)
    }

    private fun logical(expr: Logical): Any? {
        val left = evaluate(expr.left)

        if (expr.operator.type === TokenType.OR) {
            if (isTruthy(left)) return left
        } else {
            if (!isTruthy(left)) return left
        }

        return evaluate(expr.right)
    }

    private fun binary(expr: Binary): Any? {
        val left = evaluate(expr.left)
        val right = evaluate(expr.right)

        return when (expr.operator.type) {
            BANG_EQUAL -> !isEqual(left, right)
            EQUAL_EQUAL -> isEqual(left, right)
            GREATER -> {
                checkNumberOperands(expr.operator, left, right)
                left as Double > right as Double
            }
            GREATER_EQUAL -> {
                checkNumberOperands(expr.operator, left, right)
                left as Double >= right as Double
            }
            LESS -> {
                checkNumberOperands(expr.operator, left, right)
                (left as Double) < right as Double
            }
            LESS_EQUAL -> {
                checkNumberOperands(expr.operator, left, right)
                left as Double <= right as Double
            }
            MINUS -> {
                checkNumberOperands(expr.operator, left, right)
                left as Double - right as Double
            }
            PLUS -> {
                when {
                    left is Double && right is Double -> left + right
                    left is String && right is String -> left + right
                    // Unreachable.
                    else -> throw RuntimeError(
                        expr.operator,
                        "Operands must be two numbers or two strings."
                    )
                }
            }
            SLASH -> {
                checkNumberOperands(expr.operator, left, right)
                left as Double / right as Double
            }
            STAR -> {
                checkNumberOperands(expr.operator, left, right)
                left as Double * right as Double
            }
            // Unreachable.
            else -> null
        }
    }

    private fun unary(expr: Unary): Any? {
        val right = evaluate(expr.right)

        return when (expr.operator.type) {
            BANG -> !isTruthy(right)
            MINUS -> {
                checkNumberOperand(expr.operator, right)
                -(right as Double)
            }
            // Unreachable.
            else -> null
        }
    }

    private fun checkNumberOperand(operator: Token, operand: Any?) {
        if (operand is Double) return
        throw RuntimeError(operator, "Operand must be a number.")
    }

    private fun checkNumberOperands(
        operator: Token,
        left: Any?, right: Any?
    ) {
        if (left is Double && right is Double) return
        throw RuntimeError(operator, "Operands must be numbers.")
    }

    private fun isTruthy(o: Any?): Boolean = when (o) {
        null -> false
        is Boolean -> o
        else -> true
    }

    // nil is only equal to nil.
    private fun isEqual(a: Any?, b: Any?): Boolean =
        when {
            a == null && b == null -> true
            a == null -> false
            else -> a == b
        }

    private fun stringify(o: Any?): String? = when (o) {
        null -> "nil"
        // Hacky formatting
        is Double -> {
            var text = BigDecimal(o).toPlainString()
            if (text.endsWith(".0")) {
                text = text.substring(0, text.length - 2)
            }
            text
        }
        else -> o.toString()
    }
}