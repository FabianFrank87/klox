package de.pagefault.lox

class AstPrinter {
    fun print(stmts: List<Stmt>, depth: Int = 0): String =
        stmts.joinToString("\n") {
            when (it) {
                is Expression -> parenthesize("expression", it.expression, depth = depth)
                is Print -> parenthesize("print", it.expression, depth = depth)
                is Var -> if (it.initializer != null)
                    parenthesize("var ${it.name.lexeme}", it.initializer, depth = depth)
                else
                    parenthesize("var ${it.name.lexeme}", depth = depth)
                is Block -> "${"  ".repeat(depth)}{\n${
                    print(
                        it.statements,
                        depth = (depth + 1)
                    )
                }\n${"  ".repeat(depth)}}"
                is If -> parenthesize("if", it.condition, it.thenBranch, it.elseBranch, depth = depth)
                is While -> parenthesize("while", it.condition, it.body, depth = depth)
                is Function -> parenthesize(
                    "function ${it.name.lexeme}",
                    *it.params.toTypedArray(),
                    *it.body.toTypedArray()
                )
                is Return -> parenthesize("return", it.value)
                is Class -> parenthesize("class ${it.name.lexeme}", it.superclass, *it.methods.toTypedArray())
            }
        }

    fun print(expr: Expr, depth: Int = 0): String =
        when (expr) {
            is Binary -> parenthesize(expr.operator.lexeme, expr.left, expr.right, depth = depth)
            is Grouping -> parenthesize("group", expr.expression, depth = depth)
            is Literal -> expr.value?.toString() ?: "nil"
            is Unary -> parenthesize(expr.operator.lexeme, expr.right, depth = depth)
            is Variable -> parenthesize("variable ${expr.name.lexeme}", depth = depth)
            is Assign -> parenthesize("assign ${expr.name.lexeme}", expr.value, depth = depth)
            is Logical -> parenthesize(expr.operator.lexeme, expr.left, expr.right, depth = depth)
            is Call -> parenthesize("call", expr.callee, *expr.arguments.toTypedArray(), depth = depth)
            is Get -> parenthesize("get", expr.`object`, expr.name)
            is Set -> parenthesize("set", expr.`object`, expr.name, expr.value)
            is This -> parenthesize("this")
            is Super -> parenthesize("super", expr.method)
        }

    private fun parenthesize(name: String, vararg exprs: Any?, depth: Int = 0): String =
        StringBuilder().apply {
            append("  ".repeat(depth))
            append("(")
            append(name)
            for (expr in exprs) {
                append(" ")
                when (expr) {
                    is Expr -> append(print(expr, depth = depth))
                    is Stmt -> append(print(listOf(expr), depth = depth))
                    is Token -> append(expr.lexeme)
                    null -> append("null")
                    else -> throw IllegalStateException("Failed to print $expr")
                }
            }
            append(")")
        }.toString()
}

fun main(@Suppress("UNUSED_PARAMETER") args: Array<String>) {
    val expression: Expr = Binary(
        Unary(
            Token(TokenType.MINUS, "-", null, 1),
            Literal(123)
        ),
        Token(TokenType.STAR, "*", null, 1),
        Grouping(
            Literal(45.67)
        )
    )
    println(AstPrinter().print(expression))
}