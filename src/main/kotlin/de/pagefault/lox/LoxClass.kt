package de.pagefault.lox

class LoxClass(
    internal val name: String,
    private val superclass: LoxClass?,
    private val methods: Map<String, LoxFunction>
) : LoxCallable {
    override fun arity(): Int = findMethod("init")?.arity() ?: 0

    fun findMethod(name: String?): LoxFunction? = when {
        methods.containsKey(name) -> methods[name]
        superclass != null -> superclass.findMethod(name)
        else -> null
    }

    override fun call(interpreter: Interpreter, arguments: List<Any?>): Any? {
        val instance = LoxInstance(this)

        findMethod("init")?.bind(instance)?.call(interpreter, arguments)

        return instance
    }

    override fun toString(): String {
        return "<class $name>"
    }
}