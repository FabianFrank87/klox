package de.pagefault.lox

import java.util.Stack

private enum class FunctionType {
    NONE, FUNCTION, INITIALIZER, METHOD
}

private enum class ClassType {
    NONE, CLASS, SUBCLASS
}

class Resolver(private val interpreter: Interpreter) {
    private val scopes = Stack<MutableMap<String, Boolean>>()
    private var currentFunction: FunctionType = FunctionType.NONE
    private var currentClass: ClassType = ClassType.NONE

    fun resolve(statements: List<Stmt>) {
        for (statement in statements) {
            resolve(statement)
        }
    }

    private fun resolve(stmt: Stmt) {
        when (stmt) {
            is Block -> block(stmt)
            is Expression -> resolve(stmt.expression)
            is Function -> function(stmt)
            is If -> {
                resolve(stmt.condition)
                resolve(stmt.thenBranch)
                stmt.elseBranch?.let { resolve(it) }
            }
            is Print -> resolve(stmt.expression)
            is Return -> `return`(stmt)
            is Var -> `var`(stmt)
            is While -> {
                resolve(stmt.condition)
                resolve(stmt.body)
            }
            is Class -> `class`(stmt)
        }
    }

    private fun `class`(stmt: Class) {
        val enclosingClass = currentClass
        currentClass = ClassType.CLASS

        declare(stmt.name)
        define(stmt.name)

        if (stmt.superclass != null && stmt.name.lexeme == stmt.superclass.name.lexeme) {
            Lox.error(stmt.superclass.name, "A class cannot inherit from itself.")
        }

        stmt.superclass?.let {
            currentClass = ClassType.SUBCLASS
            resolve(it)

            beginScope()
            scopes.peek().put("super", true)
        }

        beginScope()
        scopes.peek()["this"] = true

        for (method in stmt.methods) {
            val functionType = if (method.name.lexeme == "init") {
                FunctionType.INITIALIZER
            } else {
                FunctionType.METHOD
            }
            resolveFunction(method, functionType)
        }

        stmt.superclass?.let { endScope() }

        currentClass = enclosingClass
        endScope()
    }

    private fun `return`(stmt: Return) {
        if (currentFunction == FunctionType.NONE) {
            Lox.error(stmt.keyword, "Cannot return from top-level code.")
        }

        if (stmt.value != null) {
            if (currentFunction == FunctionType.INITIALIZER) {
                Lox.error(stmt.keyword, "Cannot return a value from an initializer.")
            }
            resolve(stmt.value)
        }
    }

    private fun resolve(expr: Expr) {
        when (expr) {
            is Assign -> assign(expr)
            is Binary -> {
                resolve(expr.left)
                resolve(expr.right)
            }
            is Call -> {
                resolve(expr.callee)
                expr.arguments.forEach { resolve(it) }
            }
            is Grouping -> resolve(expr.expression)
            is Literal -> Unit
            is Logical -> {
                resolve(expr.left)
                resolve(expr.right)
            }
            is Unary -> resolve(expr.right)
            is Variable -> variable(expr)
            is Get -> resolve(expr.`object`)
            is Set -> {
                resolve(expr.`object`)
                resolve(expr.value)
            }
            is This -> `this`(expr)
            is Super -> {
                if (currentClass == ClassType.NONE) {
                    Lox.error(expr.keyword, "Cannot use 'super' outside of a class.")
                } else if (currentClass != ClassType.SUBCLASS) {
                    Lox.error(expr.keyword, "Cannot use 'super' in a class with no superclass.")
                }
                resolveLocal(expr, expr.keyword)
            }
        }
    }

    private fun `this`(expr: This) {
        if (currentClass == ClassType.NONE) {
            Lox.error(expr.keyword, "Cannot use 'this' outside of a class.")
        }

        resolveLocal(expr, expr.keyword)
    }

    private fun function(stmt: Function) {
        declare(stmt.name)
        define(stmt.name)

        resolveFunction(stmt, FunctionType.FUNCTION)
    }

    private fun resolveFunction(function: Function, type: FunctionType) {
        val enclosingFunction = currentFunction
        currentFunction = type
        beginScope()
        for (param in function.params) {
            declare(param)
            define(param)
        }
        resolve(function.body)
        endScope()
        currentFunction = enclosingFunction
    }

    private fun assign(expr: Assign) {
        resolve(expr.value)
        resolveLocal(expr, expr.name)
    }

    private fun variable(expr: Variable) {
        if (!scopes.isEmpty() && scopes.peek()[expr.name.lexeme] == false) {
            Lox.error(
                expr.name,
                "Cannot read local variable in its own initializer."
            )
        }

        resolveLocal(expr, expr.name)
    }

    private fun resolveLocal(expr: Expr, name: Token) {
        for (i in scopes.indices.reversed()) {
            if (scopes[i].containsKey(name.lexeme)) {
                interpreter.resolve(expr, scopes.size - 1 - i)
                return
            }
        }
        // Not found. Assume it is global.
    }

    private fun `var`(stmt: Var) {
        declare(stmt.name)
        if (stmt.initializer != null) {
            resolve(stmt.initializer)
        }
        define(stmt.name)
    }

    private fun block(stmt: Block) {
        beginScope()
        resolve(stmt.statements)
        endScope()
    }

    private fun declare(name: Token) {
        if (scopes.isEmpty()) return
        val scope: MutableMap<String, Boolean> = scopes.peek()
        if (scope.containsKey(name.lexeme)) {
            Lox.error(name, "Variable with this name already declared in this scope.")
        }
        scope[name.lexeme] = false
    }

    private fun define(name: Token) {
        if (scopes.isEmpty()) return
        scopes.peek()[name.lexeme] = true
    }

    private fun beginScope() {
        scopes.push(mutableMapOf())
    }

    private fun endScope() {
        scopes.pop()
    }
}