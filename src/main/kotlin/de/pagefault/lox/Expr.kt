package de.pagefault.lox

sealed class Expr

data class Assign(val name: Token, val value: Expr) : Expr()

data class Binary(val left: Expr, val operator: Token, val right: Expr) : Expr()

data class Call(val callee: Expr, val paren: Token, val arguments: List<Expr>) : Expr()

data class Get(val `object`: Expr, val name: Token) : Expr()

data class Grouping(val expression: Expr) : Expr()

data class Literal(val value: Any?) : Expr()

data class Logical(val left: Expr, val operator: Token, val right: Expr) : Expr()

data class Set(val `object`: Expr, val name: Token, val value: Expr) : Expr()

data class Super(val keyword: Token, val method: Token) : Expr()

data class This(val keyword: Token) : Expr()

data class Unary(val operator: Token, val right: Expr) : Expr()

data class Variable(val name: Token) : Expr()