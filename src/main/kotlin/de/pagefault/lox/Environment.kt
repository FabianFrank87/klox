package de.pagefault.lox

class Environment(
    internal val enclosing: Environment? = null
) {
    private val values: MutableMap<String, Any?> = mutableMapOf()

    @Suppress("RedundantUnitReturnType")
    fun define(name: String, value: Any?): Unit {
        values[name] = value
    }

    @Suppress("RedundantUnitReturnType")
    fun assign(name: Token, value: Any?): Unit {
        when {
            values.containsKey(name.lexeme) -> {
                values[name.lexeme] = value
            }
            enclosing != null -> {
                enclosing.assign(name, value)
            }
            else -> {
                throw RuntimeError(
                    name,
                    "Undefined variable '" + name.lexeme + "'."
                )
            }
        }
    }

    fun assignAt(distance: Int, name: Token, value: Any?) {
        ancestor(distance).values[name.lexeme] = value
    }

    operator fun get(name: Token): Any? = when {
        values.containsKey(name.lexeme) -> {
            values[name.lexeme]
        }
        enclosing != null -> {
            enclosing[name]
        }
        else -> {
            throw RuntimeError(
                name,
                "Undefined variable '" + name.lexeme + "'."
            )
        }
    }

    fun getAt(distance: Int, name: String?): Any? = ancestor(distance).values[name]

    private fun ancestor(distance: Int): Environment {
        var environment: Environment = this
        for (i in 0 until distance) {
            environment = environment.enclosing ?: throw IllegalStateException("Environment too far away $distance.")
        }
        return environment
    }
}