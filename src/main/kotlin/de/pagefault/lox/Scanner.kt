package de.pagefault.lox

import de.pagefault.lox.Lox.error
import de.pagefault.lox.TokenType.AND
import de.pagefault.lox.TokenType.BANG
import de.pagefault.lox.TokenType.BANG_EQUAL
import de.pagefault.lox.TokenType.CLASS
import de.pagefault.lox.TokenType.COMMA
import de.pagefault.lox.TokenType.DOT
import de.pagefault.lox.TokenType.ELSE
import de.pagefault.lox.TokenType.EOF
import de.pagefault.lox.TokenType.EQUAL
import de.pagefault.lox.TokenType.EQUAL_EQUAL
import de.pagefault.lox.TokenType.FALSE
import de.pagefault.lox.TokenType.FOR
import de.pagefault.lox.TokenType.FUN
import de.pagefault.lox.TokenType.GREATER
import de.pagefault.lox.TokenType.GREATER_EQUAL
import de.pagefault.lox.TokenType.IDENTIFIER
import de.pagefault.lox.TokenType.IF
import de.pagefault.lox.TokenType.LEFT_BRACE
import de.pagefault.lox.TokenType.LEFT_PAREN
import de.pagefault.lox.TokenType.LESS
import de.pagefault.lox.TokenType.LESS_EQUAL
import de.pagefault.lox.TokenType.MINUS
import de.pagefault.lox.TokenType.NIL
import de.pagefault.lox.TokenType.NUMBER
import de.pagefault.lox.TokenType.OR
import de.pagefault.lox.TokenType.PLUS
import de.pagefault.lox.TokenType.PRINT
import de.pagefault.lox.TokenType.RETURN
import de.pagefault.lox.TokenType.RIGHT_BRACE
import de.pagefault.lox.TokenType.RIGHT_PAREN
import de.pagefault.lox.TokenType.SEMICOLON
import de.pagefault.lox.TokenType.SLASH
import de.pagefault.lox.TokenType.STAR
import de.pagefault.lox.TokenType.STRING
import de.pagefault.lox.TokenType.SUPER
import de.pagefault.lox.TokenType.THIS
import de.pagefault.lox.TokenType.TRUE
import de.pagefault.lox.TokenType.VAR
import de.pagefault.lox.TokenType.WHILE

class Scanner(private val source: String) {
    private val tokens = mutableListOf<Token>()
    private var start = 0
    private var current = 0
    private var line = 1

    private val keywords = mapOf(
        "and" to AND,
        "class" to CLASS,
        "else" to ELSE,
        "false" to FALSE,
        "for" to FOR,
        "fun" to FUN,
        "if" to IF,
        "nil" to NIL,
        "or" to OR,
        "print" to PRINT,
        "return" to RETURN,
        "super" to SUPER,
        "this" to THIS,
        "true" to TRUE,
        "var" to VAR,
        "while" to WHILE
    )

    fun scanTokens(): List<Token> {
        while (!isAtEnd()) {
            // We are at the beginning of the next lexeme.
            start = current
            scanToken()
        }
        tokens.add(Token(EOF, "", null, line))
        return tokens
    }

    private fun scanToken() {
        when (val c: Char = advance()) {
            '(' -> addToken(LEFT_PAREN)
            ')' -> addToken(RIGHT_PAREN)
            '{' -> addToken(LEFT_BRACE)
            '}' -> addToken(RIGHT_BRACE)
            ',' -> addToken(COMMA)
            '.' -> addToken(DOT)
            '-' -> addToken(MINUS)
            '+' -> addToken(PLUS)
            ';' -> addToken(SEMICOLON)
            '*' -> addToken(STAR)
            '!' -> addToken(if (match('=')) BANG_EQUAL else BANG)
            '=' -> addToken(if (match('=')) EQUAL_EQUAL else EQUAL)
            '<' -> addToken(if (match('=')) LESS_EQUAL else LESS)
            '>' -> addToken(if (match('=')) GREATER_EQUAL else GREATER)

            '/' -> if (match('/')) {
                // A comment goes until the end of the line.
                while (peek() != '\n' && !isAtEnd()) advance()
            } else {
                addToken(SLASH)
            }

            // Ignore whitespace.
            ' ' -> Unit
            '\r' -> Unit
            '\t' -> Unit

            '\n' -> line++

            '"' -> string()

            else -> when {
                isDigit(c) -> number()
                isAlpha(c) -> identifier()
                else -> error(line, "Unexpected character $c.")
            }
        }
    }

    private fun identifier() {
        while (isAlphaNumeric(peek())) advance()

        // See if the identifier is a reserved word.
        val text = source.substring(start, current)

        val type = keywords[text] ?: IDENTIFIER
        addToken(type)
    }

    private fun number() {
        while (isDigit(peek())) advance()

        // Look for a fractional part.
        if (peek() == '.' && isDigit(peekNext())) {
            // Consume the "."
            advance()
            while (isDigit(peek())) advance()
        }
        addToken(NUMBER, source.substring(start, current).toDouble())
    }

    private fun string() {
        while (peek() != '"' && !isAtEnd()) {
            if (peek() == '\n') line++
            advance()
        }

        // Unterminated string.
        if (isAtEnd()) {
            error(line, "Unterminated string.")
            return
        }

        // The closing ".
        advance()

        // Trim the surrounding quotes.
        val value = source.substring(start + 1, current - 1)
        addToken(STRING, value)
    }

    private fun match(expected: Char): Boolean {
        if (isAtEnd()) return false
        if (source[current] != expected) return false
        current++
        return true
    }

    private fun peek() = if (isAtEnd()) '\u0000' else source[current]

    private fun peekNext() = if (current + 1 >= source.length) '\u0000' else source[current + 1]

    private fun isAlpha(c: Char) = c in 'a'..'z' || c in 'A'..'Z' || c == '_'

    private fun isAlphaNumeric(c: Char) = isAlpha(c) || isDigit(c)

    private fun isDigit(c: Char) = c in '0'..'9'

    private fun isAtEnd() = current >= source.length

    private fun advance(): Char {
        current++
        return source[current - 1]
    }

    private fun addToken(type: TokenType) = addToken(type, null)

    private fun addToken(type: TokenType, literal: Any?) {
        val text = source.substring(start, current)
        tokens.add(Token(type, text, literal, line))
    }
}