# Crafting Interpreters in Kotlin

Kotlin translation of chapters 1 through 13 without major modifications.

## Notes
* REPL: If last statement entered is an expression statement, it is wrapped in a print statement instead.
* debug: Global that takes a boolean parameter and turns AST printing on/off